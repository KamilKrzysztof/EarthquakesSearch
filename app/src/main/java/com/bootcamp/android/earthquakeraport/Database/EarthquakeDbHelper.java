package com.bootcamp.android.earthquakeraport.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.bootcamp.android.earthquakeraport.Database.EarthquakeContract.EarthquakeEntry;


public class EarthquakeDbHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "earthquake_1.db";
    private static final int DATABASE_VERSION = 1;

    public EarthquakeDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_POSITION_TABLE = "CREATE TABLE " + EarthquakeEntry.TABLE_NAME + " ("
                + EarthquakeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EarthquakeEntry.COLUMN_EARTHQUAKE_LOCATION + " TEXT, "
                + EarthquakeEntry.COLUMN_EARTHQUAKE_DATE + " TEXT, "
                + EarthquakeEntry.COLUMN_EARTHQUAKE_MAGNITUDE + " TEXT " + ");";
        db.execSQL(SQL_CREATE_POSITION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
