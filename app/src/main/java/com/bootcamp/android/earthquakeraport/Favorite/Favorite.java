package com.bootcamp.android.earthquakeraport.Favorite;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bootcamp.android.earthquakeraport.R;
import com.bootcamp.android.earthquakeraport.Database.EarthquakeContract;

import java.util.ArrayList;

public class Favorite extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private ArrayList<String> arrayList;
    private ListView listFavorites;
    private ArrayAdapter<String> arrayAdapter;
    private EarthquakeFavoriteAdapter earthquakeFavoriteAdapter;
    private TextView locationFavorite;
    private TextView magnetudeFavorite;
    private TextView timeFavorite;
    private Uri mCurrentEarthquakeUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        listFavorites = (ListView) findViewById(R.id.list_favorites);
        earthquakeFavoriteAdapter = new EarthquakeFavoriteAdapter(this, null);
        listFavorites.setAdapter(earthquakeFavoriteAdapter);

        Intent intent = getIntent();
        mCurrentEarthquakeUri = intent.getData();

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_all_positions:
                showDeleteConfirmationDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                EarthquakeContract.EarthquakeEntry._ID,
                EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_LOCATION,
                EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_MAGNITUDE,
                EarthquakeContract.EarthquakeEntry.COLUMN_EARTHQUAKE_DATE,
        };

        return new CursorLoader(this,
                EarthquakeContract.EarthquakeEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        earthquakeFavoriteAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        earthquakeFavoriteAdapter.swapCursor(null);
    }

    private void deleteAllPositions(){
        int rowsDeleted = getContentResolver().delete(EarthquakeContract.EarthquakeEntry.CONTENT_URI, null, null);
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete this location?");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteAllPositions();
            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
