package com.bootcamp.android.earthquakeraport.Database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bootcamp.android.earthquakeraport.Database.EarthquakeContract.EarthquakeEntry;

public class EarthquakeProvider extends ContentProvider{

    private static final int EARTHQUAKES = 100;
    private static final int EARTHQUAKE_ID = 101;
    private EarthquakeDbHelper dbHelper;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(EarthquakeContract.CONTENT_AUTHORITY, EarthquakeContract.PATH_EARTHQUAKE, EARTHQUAKES);
        sUriMatcher.addURI(EarthquakeContract.CONTENT_AUTHORITY, EarthquakeContract.PATH_EARTHQUAKE+ "/#", EARTHQUAKE_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new EarthquakeDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor;
        int match = sUriMatcher.match(uri);
        switch (match){
            case EARTHQUAKES:
                cursor = database.query(EarthquakeEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case EARTHQUAKE_ID:
                selection = EarthquakeEntry._ID+"=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(EarthquakeEntry.TABLE_NAME,
                        projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Query error for: "+uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }



    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match){
            case EARTHQUAKES:
                SQLiteDatabase database = dbHelper.getWritableDatabase();

                long id = database.insert(EarthquakeEntry.TABLE_NAME, null, contentValues);
                if (id == -1) {
                    return null;
                }

                getContext().getContentResolver().notifyChange(uri, null);
                return ContentUris.withAppendedId(uri, id);

            default:
                throw new IllegalArgumentException("Insertion error for: " + uri);
        }
    }


    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match){
            case EARTHQUAKES:
                return updatePosition(uri, contentValues, selection, selectionArgs);

            case EARTHQUAKE_ID:
                selection = EarthquakeEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updatePosition(uri, contentValues, selection, selectionArgs);
            default:
                throw new IllegalArgumentException("Update error for: " + uri);
        }
    }

    private int updatePosition(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs){
        if (contentValues.size()==0){
            return 0;
        }
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        int rowsUpdated = database.update(EarthquakeEntry.TABLE_NAME,
                contentValues, selection, selectionArgs);
        if(rowsUpdated !=0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        int rowsDeleted;
        final int match = sUriMatcher.match(uri);
        switch (match){
            case EARTHQUAKES:
                rowsDeleted = database.delete(EarthquakeEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            case EARTHQUAKE_ID:
                selection = EarthquakeEntry._ID+"=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                rowsDeleted = database.delete(EarthquakeEntry.TABLE_NAME,
                        selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Deletion error for: " + uri);
        }
        if (rowsDeleted !=0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match){
            case EARTHQUAKES:
                return EarthquakeEntry.CONTENT_LIST_TYPE;
            case EARTHQUAKE_ID:
                return EarthquakeEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }
}
